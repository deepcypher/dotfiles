DeepCypher Dotfiles
===================

This repository contains DeepCypher styled dotfiles for consistent theming across environments.
Powered by Makefiles, stow, or chezmoi. Currently for x11.

Preview
-------

Chezmoi Documentation
---------------------

.. note::

   It is strongly advised that you also refer to https://www.chezmoi.io/user-guide

Requirements
------------

* i3wm-gaps
* chezmoi
* git
* bash
* fish
* polybar
* rofi
* papirus-icon-theme

Init
----

If you are using chezmoi, add this repositroy:

.. code-block:: bash

   chezmoi init https://gitlab.com/deepcypher/dotfiles.git

OR (with local makefile)

.. code-block:: bash

  make init

Update Source Manifests
-----------------------

You may find that in time you want to pull in some changes from the remote repository you can use chezmoi as a go-between with git i.e:

.. code-block:: bash

   chezmoi git pull

OR (with local makefile)

.. code-block:: bash

  make update

Display diff
------------

To preview changes that will be made by chezmoi:

.. code-block:: bash

   chezmoi diff

OR (with local makefile)

.. code-block:: bash

  make diff

Install
-------

.. code-block:: bash

   chezmoi apply

OR (with local makefile)

.. code-block:: bash

  make install

Uninstall
---------

.. code-block:: bash

   chezmoi

OR (with local makefile)

.. code-block:: bash

  make uninstall
