REPO=https://gitlab.com/deepcypher/dotfiles.git
# every double comment will be used to auto generate help messages

.PHONY: help
help: ## display this auto generated help message
	@echo "Please provide a make target:"
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

.PHONY: init
init: ## initialise chezmoi source to use https://gitlab.com/deepcypher/dotfiles.git
	chezmoi init ${REPO} --debug

.PHONY: install
install: ## install currentley pulled source version (no update)
	chezmoi apply --debug

.PHONY: update
update: ## reconcile local source with remote source
	chezmoi --debug git pull

.PHONY: status
status: ## show what would be applied with apply
	chezmoi status --debug

.PHONY: diff
diff: ## show diff between local source and current configuration
	chezmoi diff --debug

.PHONY: merge
merge: ## merge local source with currently running configuration
	chezmoi merge --debug

.PHONY: upgrade
upgrade: update install ## reconcile against remote and install

# .PHONY: uninstall
# uninstall: ## uninstall configuration installed by this utility
# 	chezmoi
