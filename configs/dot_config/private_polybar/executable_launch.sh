#!/usr/bin/env bash
killall -q polybar
for m in $(polybar --list-monitors | cut -d":" -f1); do
    MONITOR=$m polybar --reload wunderbar &
done
echo "Polybar launched..."

# if type "xrandr"; then
#   for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
#     MONITOR=$m polybar --reload example -c $HOME/.config/polybar/config.ini &
#   done
# else
#   polybar example -c $HOME/.config/polybar/config.ini &
# fi
